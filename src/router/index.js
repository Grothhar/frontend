import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import BindingExample from '@/guide/BindingExample'
import ChatHistory from '@/guide/ChatHistory'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/binding-example',
      name: 'Binding Example',
      component: BindingExample
    },
    {
      path: '/chat-history',
      name: 'Chat History',
      component: ChatHistory
    }
  ]
})
